using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilCont : MonoBehaviour
{
    public Vector3 Vel;
    public float Speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Mover()
    {
        StartCoroutine(MoverDisparo());
    }
    public IEnumerator MoverDisparo() {        
        float tiempo = 0.5f;
        while (tiempo > 0) {
            RaycastHit R;
            if (Physics.Raycast(transform.position, Vel, out R, Speed * Time.deltaTime))
            {
                //Hit
                Debug.Log("Hit con: " + R.transform.name);
                if (R.transform.tag == "Player") {
                    //Da�o al jugador
                    R.transform.GetComponent<PlayerController>().RecibirDa�o(10);
                }
                else if (R.transform.tag == "Monster")
                {
                    //Da�o colateral al monstruo
                }
                Reset();
            }
            else {
                transform.Translate(Vel.normalized*Speed*Time.deltaTime);
            }
            tiempo -= Time.deltaTime;
            yield return null;
        }
        yield return null;
    }
    public void Reset()
    {
        //Reiniciar bala
        gameObject.SetActive(false);
        Vel = new Vector3(0,0,0);
        transform.position = new Vector3(0, 0, 0);
    }
}
