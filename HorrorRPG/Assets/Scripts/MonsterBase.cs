using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

public class MonsterBase : MonoBehaviour
{
    public PlayerController Player;
    public Vector3 PreviousPosPlayer;
    public Sprite[] Sprites;
    private SpriteRenderer Rend;
    private Camera CamaraPlayer;
    public NavMeshPath p;
    public NavMeshObstacle collision;
    private NavMeshHit hit;
    private bool blocked = false;
    //Piezas
    public GameObject AttAlert;
    public CapsuleCollider Col;
    public TraumaInducer Shaker;
    //Proyectiles
    public GameObject BolsaProyectiles;
    public GameObject MiBala;
    public ProyectilCont[] Proyectiles;
    public GameObject Ca�on;
    //Estados
    public float AngToPlayer;
    public enum Estados { Pasivo, Patrulla, Alertado, KO, Atacando};
    public Estados miestado;
    public MonsterAttack ma = null; //El ataque que aspira a realizar
    public List<MonsterBuff> Bufos;
    //Estadisticas
    public List<Vector3> Camino;
    
    public bool bVolador;
    public bool bMoviendo;
    public int Prio;
    public float MoveSpeed;
    public float RotSpeed;
    public MonsterAttack[] Ataques;
    public int Fuerza;
    public int Agallas;
    public int Percepcion;
    public int Agilidad;
    public int AngAtaque;
    public int Velocidad;
    public List<GameObject> ObjetosEnRango;

    private void Awake()
    {
        BolsaProyectiles = GameObject.Find("BolsaProyectiles");
        Preparaci�nMonstruo();
    }


    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player").GetComponent<PlayerController>();        
        Rend = GetComponent<SpriteRenderer>();
        CamaraPlayer = Camera.main;
        PlayerController.EndOfturn.AddListener(FinDeTurno);
        PlayerController.MonsterReport.AddListener(MonsterRep);       
        p = new NavMeshPath();
       
    }

    // Update is called once per frame
    void Update()    {

        //Calcula el angulo con respecto al jugador para cambiar su sprite segun desde donde lo est� mirando el jugador 
        Vector3 v = Player.transform.position - transform.position;
        AngToPlayer = Vector3.Angle(transform.forward, v);
        //De frente
        if (AngToPlayer >= 0 && AngToPlayer <= 45) {
            Rend.sprite = Sprites[0];
        }
        //De lado
        else if (AngToPlayer > 45 && AngToPlayer <= 135)
        {
            Rend.sprite = Sprites[1];
        }
        //Por detr�s
        else if (AngToPlayer > 135 && AngToPlayer <= 180)
        {
            Rend.sprite = Sprites[2];
        }
        //Acciones
        if (miestado == Estados.Pasivo) {
            Alert();
        }
    }
    public void Preparaci�nMonstruo() {
        for (int i = 0; i < Proyectiles.Length; i++)
        {
            Proyectiles[i] = Instantiate(MiBala).GetComponent<ProyectilCont>();
            Proyectiles[i].transform.SetParent(BolsaProyectiles.transform);
            Proyectiles[i].transform.position = new Vector3(0, 0, 0);
            Proyectiles[i].transform.rotation = Quaternion.identity;
        }
    }
    public void Alert() {
        miestado = Estados.Alertado;
        StartCoroutine(CalcularAccion());
    }
    public void MonsterRep()
    {
        
        if (gameObject.activeSelf) {
            Debug.Log(gameObject.name + " informando");
            Player.Monsters.Add(this);
           
        }
       
    }
    public void FinDeTurno()
    {
        
        StartCoroutine(EndOfTurnMonster());
    }
    public IEnumerator EndOfTurnMonster()
    {
        
        //Debug.LogWarning(gameObject.name + " Fin Del turno?");
        yield return null;
        
    }
    public IEnumerator CalcularAccion()
    {
        Vector3 v = Player.transform.position - transform.position;
        Camino.Clear();
        int layermask = LayerMask.GetMask("Player");
        collision.enabled = false;
        yield return null;
        if (miestado == Estados.Pasivo)
        {

        }
        if (miestado == Estados.Atacando)
        {
            collision.enabled = true;
        }
        if (miestado == Estados.Alertado)
        {
            //Decidir el ataque a realizar
            int r = Random.Range(1, 11);
            if (ma.Tipo == MonsterAttack.Type.none)
            { //Si no tiene un ataque pensado, elige uno
                for (int x = 0; x < Ataques.Length; x++)
                {
                    if (ma == null && Ataques[x].prio < r)
                    {
                        ma = Ataques[x];
                    }
                    else if (Ataques[x].prio > ma.prio && Ataques[x].prio <= r)
                    {
                        ma = Ataques[x];
                    }
                }
            }
            
            //Si llega a realizar el ataque, lo prepara directamente para el siguiente turno
            if (v.magnitude <= ma.distance && AngToPlayer <= ma.angle)
            {
                PrepararAtaque();
                collision.enabled = true;
                Debug.Log("Colision: " + collision.gameObject.activeSelf);
                miestado = Estados.Atacando;
                yield return null;
            }
            else
            {
                    //Si no llega intenta moverse para colocarse a distancia
                    //Camino a recorrer
                    RaycastHit hit;
                    blocked = Physics.Raycast(transform.position, v, out hit, v.magnitude, layermask);
                    Debug.DrawRay(transform.position, v, blocked ? Color.red : Color.green, 1);
                    if (blocked)
                        Debug.DrawRay(hit.point - (v.normalized * collision.radius), Vector3.up, Color.red, 1);
                    if (NavMesh.CalculatePath(transform.position, hit.point - (v.normalized * collision.radius), NavMesh.AllAreas, p))
                    {
                        float distancia = Velocidad * MoveSpeed;
                        float currangle = transform.rotation.y;
                        Vector3 previosPos = transform.position;
                        Vector3 ori = transform.forward;
                        Vector3 v2;
                        for (int i = 1; i < p.corners.Length; i++)
                        {
                            if (i == 1)
                            {
                                v2 = p.corners[i] - transform.position;

                            }
                            else
                            {
                                v2 = p.corners[i] - p.corners[i - 1];
                                //Debug.DrawLine(p.corners[i - 1], p.corners[i], Color.magenta, 2);
                            }
                            float angle = Vector3.Angle(ori, v2);
                            //Debug.Log("Paso - " + i + " - " + transform.name + " tiene para caminar " + distancia + " y tiene que girar " + angle);
                            distancia -= (angle / (100 * RotSpeed));
                            distancia -= v2.magnitude;
                            if (distancia >= 0)
                            {
                                Camino.Add(p.corners[i]);
                                Debug.DrawLine(previosPos, p.corners[i], Color.magenta, 2);
                                previosPos = p.corners[i];
                            }
                            else
                            {
                                Vector3 f = (p.corners[i] + (v2.normalized * distancia));
                                Camino.Add(f);
                                Debug.DrawLine(previosPos, f, Color.magenta, 2);
                                previosPos = f;
                                break;
                            }

                            ori = v2;
                        }
                        Debug.Log(gameObject.name + " path calculado");
                    }
                    else
                    {
                        Debug.Log(gameObject.name + " path no encontrado");
                    }
                }
            collision.enabled = true;
            yield return null;
            
        }
    }
    public void PrepararAtaque() {
        //Si el monstruo puede atacar preparar� el ataque para el turno siguiente
        miestado = Estados.Atacando;
        Debug.LogError(gameObject.name + " est� preparando un ataque!");
        
    }
    public void RealizarAtaque() {
        int layermask = LayerMask.GetMask("Obstaculos", "Player", "Monstruos");
        ObjetosEnRango.Clear();
        
        if (ma.Tipo == MonsterAttack.Type.melee || ma.Tipo == MonsterAttack.Type.blitz) {
           
            Collider[] c;
            c = Physics.OverlapSphere(transform.position, ma.distance, layermask);
            for (int i = 0; i < c.Length; i++)
            {
                Vector3 v = c[i].transform.position - transform.position;
                float a = Vector3.Angle(transform.forward, v);
                if (a <= ma.angle)
                {
                    if (c[i] != Col) {
                        ObjetosEnRango.Add(c[i].gameObject);
                    }
                    
                }
            }
            
            //Haciendo el da�o alos objetivos
            for (int i = 0; i < ObjetosEnRango.Count; i++)
            {
                
                if (ObjetosEnRango[i].tag == "Player") {                   
                    ObjetosEnRango[i].GetComponent<PlayerController>().RecibirDa�o(10);
                    
                }
                //ObjetosEnRango[i]
            }
        }
        if (ma.Tipo == MonsterAttack.Type.ranged)
        {
            for (int i = 0; i < Proyectiles.Length; i++)
            {
                if (!Proyectiles[i].gameObject.activeSelf) {
                    Proyectiles[i].gameObject.SetActive(true);
                    Proyectiles[i].transform.position = Ca�on.transform.position;
                    Proyectiles[i].Vel = PreviousPosPlayer - transform.position;
                    Proyectiles[i].Mover();
                    break;
                }
            }
        }
    }
    public void GetBuff(MonsterBuff b) {
        Bufos.Add(b);
    }
    public void ObligarMovimiento() {
        StartCoroutine(Mover());
    }
    public IEnumerator Mover() {
        bMoviendo = true;
        //Mover Los Proyectiles
        for (int i = 0; i < Proyectiles.Length; i++)
        {
            if (Proyectiles[i].gameObject.activeSelf) {
                Proyectiles[i].Mover();
            }
        }
        if (miestado != Estados.Atacando)
        {
            //Debug.Log(gameObject.name + " se est� moviendo");
            Vector3 v2;
            int bDerecha;
            for (int i = 0; i < Camino.Count; i++)
            {
                //rotar

                if (i == 0)
                {
                    v2 = Camino[i] - transform.position;
                    //Debug.Log(gameObject.name + " est� rotando " + v2);
                }
                else
                {
                    v2 = Camino[i] - Camino[i - 1];
                    //Debug.DrawLine(p.corners[i - 1], p.corners[i], Color.magenta, 2);
                    //Debug.Log(gameObject.name + " est� rotando " + v2);
                }
                if (v2.x >= 0) { bDerecha = 1; }
                else { bDerecha = 1; }
                float angle = Vector3.Angle(transform.forward, v2);
                //El angulo con el que debe quedar
                //Debug.Log("Rotacion estimada " + Quaternion.LookRotation(v2, transform.forward));
                float t = 0.5f;
                Quaternion q = transform.rotation;
                while (t > 0)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(v2, Vector3.up), 1-(t/0.5f));
                    t -= Time.deltaTime;

                    yield return null;
                }
                transform.rotation = Quaternion.LookRotation(v2, Vector3.up);
                //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(v2, Vector3.up), 0);
                yield return null;
                //Moverse hasta la posici�n final
                Vector3 m = (Camino[i] - transform.position);
                bool bat = false; //si ha interrumpido el movimiento
                float vel = 2.5f; //Velocidad de mov base
                if (ma.Tipo == MonsterAttack.Type.blitz)
                {
                    vel = 10f; //Velocidad de mov de ataque relampago
                }
                while (m.magnitude >= 0.25f && !bat)
                {
                    transform.Translate(m.normalized * Time.deltaTime * vel, Space.World);
                    m = (Camino[i] - transform.position);
                    Vector3 v = Player.transform.position - transform.position;
                    if (v.magnitude <= ma.distance * 1 && AngToPlayer <= ma.angle && ma.Tipo != MonsterAttack.Type.blitz)
                    {
                        RaycastHit I;
                        if (Physics.Raycast(Ca�on.transform.position, Player.transform.position - Ca�on.transform.position, out I, (Player.transform.position - Ca�on.transform.position).magnitude)){
                            if (I.transform.tag == "Player") {
                                bat = true;
                                //Llega a distancia de ataque preferente y prepara un ataque
                                PrepararAtaque();
                            }
                        }
                       
                    }
                    if (v.magnitude <= ma.distance * 1 && AngToPlayer <= ma.angle && ma.Tipo == MonsterAttack.Type.blitz)
                    {
                        RaycastHit I;
                        if (Physics.Raycast(Ca�on.transform.position, Player.transform.position - Ca�on.transform.position, out I, (Player.transform.position - Ca�on.transform.position).magnitude))
                        {
                            if (I.transform.tag == "Player")
                            {
                                bat = true;
                                //Llega a distancia de ataque preferente y realiza el ataque relampago
                                RealizarAtaque();
                                yield return new WaitForSeconds(1);
                                miestado = Estados.Alertado;
                                ma = new MonsterAttack(); //Se olvida de su ataque recien realizado
                                Debug.Log("Realizado ataque relampago");
                            }
                        }

                    }
                    //Deteccion de obstaculos inesperados (otros monstruos) puede fallar mas adelante si detecta obstaculos prematuramente
                    RaycastHit H;
                    if (Physics.Raycast(transform.position, m.normalized, out H, Col.radius * 2.5f))
                    {
                        Debug.DrawRay(transform.position, m.normalized * Col.radius * 2, Color.yellow, 2.5f);
                        bat = true;
                    }
                    yield return null;
                }
                if (ma.Tipo == MonsterAttack.Type.blitz)
                {
                    Debug.Log("Realizado ataque relampago sin exito");
                }
                if (!bat)
                {
                    transform.position = Camino[i];
                }



            }
        }
        else {
            //El Monstruo ataca aqui
            RealizarAtaque();
            yield return new WaitForSeconds(1);
            miestado = Estados.Alertado;
            ma = new MonsterAttack(); //Se olvida de su ataque recien realizado
            Debug.Log("Realizado ataque");
        }
        
        StartCoroutine(CalcularAccion());
        PreviousPosPlayer = Player.transform.position;
        yield return null;        
        bMoviendo = false;
    }
}
[System.Serializable]
public class MonsterAttack{
    public float distance;
    public float angle;
    public float Da�oBase;
    public enum Type {none, melee, ranged, gun, blitz };
    public float prio;
    public Type Tipo;
    public bool FriendlyFire;
}
[System.Serializable]
public class MonsterBuff
{
    public int turns;
    public int Fuerza;
    public int Agallas;
    public int Percepcion;
    public int Agilidad;
    public int Velocidad;
}
