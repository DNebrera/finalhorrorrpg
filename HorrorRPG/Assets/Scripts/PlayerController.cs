using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using UnityEngine.AI;


public class PlayerController : MonoBehaviour
{
    //Controles
    [Header("Controles")]
    public NavMeshPath p;
    public Vector3[] Camino;
    public GameObject Waypoint;
    public GameObject WaypointFinal;
    public enum FasesFinTurno {Ninguna, Prep, PreMovMon, PreAtaqueMon, Mov, Ataque,MovMon,AtaqueMon,Final};
    public FasesFinTurno FasesFinales;
    public bool bControl;
    public bool bCheckingPath;
    public float TimetoCheck;
    private float MouseVert;
    public float timeTMoveF;
    public float timeTMoveL;
    public float timeTMoveR;
    //Stats
    [Header("Stats")]
    public float Vida;    
    public int Ansiedad;
    public float MoveSpeed;     
    public int Fuerza;
    public int Agallas;
    public int Percepcion;
    public int Agilidad;
    public int Conocimiento;
    public int Velocidad;
    public Vector3 Mov;
    //InventoryMenu
    [Header("Inventario")]
    public GameObject InventoryMenu;
    public Image HpImage;
    public GradientColorKey[] VidaGradient;
    public GradientAlphaKey[] VidaGradientAlfa;
    public Gradient ColorVidaGradient;
    //Opciones
    [Header("Opciones e usabilidad")]
    public float Sensibilidad;
    //Piezas
    [Header("Partes")]
    public Camera Cam;
    public LineRenderer Marcamino;
    public Rigidbody CamHolder;
    public GameObject Pos;
    public StressReceiver Shaker;
    public Image FArrow;
    public Image LArrow;
    public Image RArrow;
    public Image[] AlertasUI;
    //Testeo
    [Header("Testeo")]
    public TextMeshProUGUI TextoQA;
    public Vector3 TestVector;
    public Vector3 TestVector2;
    public static UnityEvent EndOfturn = new UnityEvent();
    public static UnityEvent MonsterReport = new UnityEvent(); //Le pide a los monstruos aactivos que informen de su existencia
    //Monstruos y otros
    public List<MonsterBase> Monsters; // Los monstruos de laa zona
    public List<MonsterBase> TempMonsters;
    // Start is called before the first frame update
    void Start()
    {
        EndOfturn.AddListener(FinDeTurno);
        Cursor.lockState = CursorLockMode.Locked;
        ColorVidaGradient = new Gradient();
        VidaGradient = new GradientColorKey[3];
        VidaGradientAlfa = new GradientAlphaKey[2];
        VidaGradient[0].color = Color.red;
        VidaGradient[0].time = 0.0f;
        VidaGradient[1].color = Color.white;
        VidaGradient[1].time = 1.0f;
        VidaGradient[2].color = Color.green;
        VidaGradient[2].time = 0.80f;
        VidaGradientAlfa[0].alpha = 1;
        VidaGradientAlfa[0].time = 0.0f;
        VidaGradientAlfa[1].alpha = 1;
        VidaGradientAlfa[1].time = 1.0f;
        ColorVidaGradient.SetKeys(VidaGradient, VidaGradientAlfa);
        Application.targetFrameRate = 60;
        
        p = new NavMeshPath();
        StartCoroutine(PrepararZona());
    }

    // Update is called once per frame
    void Update()
    {
        float Mousex = Input.GetAxis("Mouse X") * Sensibilidad * Time.deltaTime * 100;
        transform.Rotate(new Vector3(0, Mousex, 0));
        float Mousey = Input.GetAxis("Mouse Y") * Sensibilidad * Time.deltaTime * 100;
        MouseVert -= Mousey;
        MouseVert = Mathf.Clamp(MouseVert, -75, 75);
        Cam.transform.localRotation = Quaternion.Euler(MouseVert, 0, 0);
        FArrow.fillAmount = (timeTMoveF / 0.7f) * 1.5f;
        LArrow.fillAmount = (timeTMoveL / 0.7f) * 1.5f;
        //Boton de pruebas
        if (Input.GetKeyDown(KeyCode.P))
        {
            TestVector = transform.position - Monsters[0].transform.position;
            CamHolder.AddTorque(new Vector3(0,TestVector.z,0) * 10000);
           

        }
        TextoQA.text = "FPS - " + (int)(1f / Time.unscaledDeltaTime);
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            bCheckingPath = true;

        }
        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            bCheckingPath = false;
            Camino = new Vector3[0];
            WaypointFinal.transform.position = transform.position;
            WaypointFinal.SetActive(false);
            TimetoCheck = 1;
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                InventoryMenu.SetActive(true);
            }
            else {
                Time.timeScale = 1;
                InventoryMenu.SetActive(false);
            }
        }
        if (bControl) {
            //Movimiento adelante con carga de flecha de movimiento
            if (Input.GetAxis("Vertical") >= 0.6f)
            {

                timeTMoveF += Time.deltaTime;
                if (timeTMoveF >= 0.75f) {
                    ComprobarMovimiento(1);
                    timeTMoveF = 0;
                }
            }
            if (timeTMoveF > 0f && Input.GetAxis("Vertical") <= 0.4f) {
                timeTMoveF = 0;
            }
            //Movimiento izquierda con carga de flecha de movimiento
            if (Input.GetAxis("Horizontal") <= -0.6f)
            {

                timeTMoveL += Time.deltaTime;
                if (timeTMoveL >= 0.75f)
                {
                    ComprobarMovimiento(4);
                    timeTMoveL = 0;
                }
            }
            if (timeTMoveL > 0f && Input.GetAxis("Horizontal") >= -0.4f)
            {
                timeTMoveL = 0;
            }
            //Fuerza el fin del turno
            if (Input.GetKeyDown(KeyCode.Q))
            {
                EndOfturn.Invoke();
            }
            //Modo Mover con precisi�n
            if (bCheckingPath)
            {
                WaypointFinal.SetActive(true);
                Waypoint.SetActive(true);
                Marcamino.gameObject.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {                                      
                    TimetoCheck = 1;
                    WaypointFinal.SetActive(false);
                    Waypoint.SetActive(false);
                    Marcamino.positionCount = 0;
                    Marcamino.gameObject.SetActive(false);
                    ComprobarMovimiento(5);
                }
               
                TimetoCheck += Time.deltaTime;
                Debug.DrawRay(Cam.transform.position, Cam.transform.forward * (Velocidad * MoveSpeed), Color.green, 0);
                if (TimetoCheck >= 0.5f)
                {
                    Waypoint.transform.position = transform.position;
                    int layermask = LayerMask.GetMask("Impasables");
                    RaycastHit R;
                    RaycastHit R2;
                    if (Physics.Raycast(Cam.transform.position, Cam.transform.forward, out R, (Velocidad * MoveSpeed), layermask))
                    {
                        //Coloca el waypoint donde impacta la linea de visi�n
                        Waypoint.transform.position = R.point;
                        if (Physics.Raycast(Waypoint.transform.position, -Waypoint.transform.up, out R2, 100, layermask))
                        {
                            //Comprueba el suelo y coloca el waaypoint ahi
                            Waypoint.transform.position = new Vector3(R2.point.x, R2.point.y, R2.point.z);
                        }
                        FindPath();


                    }
                    else
                    {
                        Waypoint.transform.position += Cam.transform.forward * (Velocidad * MoveSpeed);
                        RaycastHit R3;
                        if (Physics.Raycast(Waypoint.transform.position, -Waypoint.transform.up, out R3, 100, layermask))
                        {
                            Waypoint.transform.position = R3.point;
                        }
                        FindPath();
                    }
                    
                    TimetoCheck = 0;

                }


            }
            else {
                WaypointFinal.SetActive(false);
                Waypoint.SetActive(false);
                Marcamino.gameObject.SetActive(false);
            }
            for (int i2 = 0; i2 < AlertasUI.Length; i2++)
            {
                AlertasUI[i2].gameObject.SetActive(false);
            }
            for (int i = 0; i < Monsters.Count; i++)
            {
                if (Monsters[i].miestado == MonsterBase.Estados.Atacando || Monsters[i].ma.Tipo == MonsterAttack.Type.blitz)
                {
                    Vector3 camP = Cam.WorldToScreenPoint(Monsters[i].transform.position);
                    Vector3 camP2 = Monsters[i].transform.position - transform.position;                    
                    //comprueba si excede la pantalla
                    if (camP.z >= 0)
                    {
                        if (camP.x <= 0)
                        {
                            camP.x = 0;
                        }
                        else if (camP.x >= Screen.width)
                        {
                            camP.x = Screen.width;
                        }
                        if (camP.y <= 0)
                        {
                            camP.y = 0;
                        }
                        else if (camP.y >= Screen.height)
                        {
                            camP.y = Screen.height;
                        }
                        for (int i2 = 0; i2 < AlertasUI.Length; i2++)
                        {
                            if (!AlertasUI[i2].gameObject.activeSelf) {
                                AlertasUI[i2].gameObject.SetActive(true);
                                AlertasUI[i2].transform.position = camP;
                                break;
                            }
                        }
                       
                    }
                    else {
                        
                    }              
                   
                    
                    
                    

                }
            }
           
        }
        
        
    }
    public void RecibirDa�o(int d) {
        Debug.LogError("Da�o recibido - " + d);
        Vida -= d;
        Ansiedad += d * 2;
        //Shake
        Shaker.InduceStress(d);
        HpImage.color = ColorVidaGradient.Evaluate(Vida / 100);
    }
    //Comprobar los monstruos que hay en la zona
    public void CheckMonsters()
    {
        Monsters.Clear();
        MonsterReport.Invoke();
        Debug.Log("Monstruos Informad");
    }
    public void FindPath() {
        
        //Si se encuentra un camino al waypoint calcular� cuanto de ese camino puede recorrer segun su movimiento
        if (NavMesh.CalculatePath(transform.position, Waypoint.transform.position, NavMesh.AllAreas, p))
        {
            Camino = p.corners;            
            float Distancia = Velocidad * MoveSpeed;
            //Debug.Log("Path encontrado " + Distancia);
            Vector3 posAct = transform.position;
            // Para Cada punto en el camino calcula si consumiendo la distancia por recorrer restante llegar�
            for (int i = 0; i < Camino.Length; i++)
            {
                //Debug.Log(i + " - Pos Actual est� en - " + posAct);                
                Vector3 r = Camino[i] - posAct;                
                if (Distancia > r.magnitude)
                {
                    Debug.DrawLine(posAct, Camino[i], Color.blue, 1);
                    //Debug.Log("Punto de ruta " + i + " desde - " + posAct + " hasta - " + Camino[i] + " falta " + Distancia + " por recorrer, con distancia hasta alli de: " + r.magnitude);

                    Distancia -= r.magnitude;
                    posAct = Camino[i];
                }
                //Si no llega al punto, coloca la posici�n final en el punto maximo del recorrido al que puede acceder
                else
                {
                    Debug.DrawLine(posAct, Camino[i], Color.red, 1);
                    //Debug.LogError("Se pasa en el punto - " + i + " necesita recorrer una distancia de: " + r.magnitude + " y le queda - " + Distancia);
                    float resto = r.magnitude - Distancia;
                    Vector3 f = posAct += r.normalized * Distancia;
                    Debug.DrawLine(posAct, f, Color.blue, 0.5f);
                    //Debug.LogWarning("Path localizado " + i + " en " + posAct + " con distancia restante - " + Distancia);                    
                    break;
                }

            }
            //Tras definir la posicion final, marca el waypoint final ahi para tener referencia de hasta donde queremos llegar
            Marcamino.positionCount = Camino.Length;
            Marcamino.SetPositions(Camino);
            WaypointFinal.transform.position = posAct;
        }
        else {
            //Aqui colocaaremos laa notificaci�n de que ese punto es inaccesible
            //Debug.Log("Path no encontrado");
        }
    }    
    public void FinDeTurno()
    {
        Debug.LogWarning(gameObject.name + " Fin Del turno?");
        FasesFinales = FasesFinTurno.Prep;
        StartCoroutine(EndOfTurnPlayer());
        
    }

    //La corrutina que ejecuta el final del turno. El punto pivotal del juego.
    public IEnumerator EndOfTurnPlayer() {
        
        //Preparaci�n del fin del turno        
        while (FasesFinales == FasesFinTurno.Prep) {
            yield return null;
            FasesFinales = FasesFinTurno.PreMovMon;            
        }
        while (FasesFinales == FasesFinTurno.PreMovMon)
        {
            yield return null;
            FasesFinales = FasesFinTurno.PreAtaqueMon;
        }
        while (FasesFinales == FasesFinTurno.PreAtaqueMon)
        {
            yield return null;
            FasesFinales = FasesFinTurno.Mov;
        }
        while (FasesFinales == FasesFinTurno.Mov)
        {
            for (int i = 1; i < Camino.Length; i++)
            {
                Vector3 m = (Camino[i] - transform.position);
                while (m.magnitude >= 0.1f)
                {
                    transform.Translate(m.normalized * Time.deltaTime * 5, Space.World);
                    m = (Camino[i] - transform.position);
                    //transform.Translate(transform.forward * Time.deltaTime * 10);
                    yield return null;
                }
                transform.position = Camino[i];
            }
            yield return null;
            FasesFinales = FasesFinTurno.Ataque;
        }
        while (FasesFinales == FasesFinTurno.Ataque)
        {
            yield return null;
            FasesFinales = FasesFinTurno.MovMon;
        }
        while (FasesFinales == FasesFinTurno.MovMon)
        {
            
            
            List<MonsterBase> tempMonsters = new List<MonsterBase>();
            foreach (MonsterBase mo in Monsters) {
                TempMonsters.Add(mo);
                
            }
            int mn = Monsters.Count;
            for (int i2 = 0; i2 < mn; i2++)
            {
                MonsterBase m = null;
                int prio = 0;
                Debug.Log("Comienzo del movimieno de " + m);
                for (int i = 0; i < TempMonsters.Count; i++)
                {
                    if (TempMonsters[i].Prio > prio)
                    {
                        m = TempMonsters[i];
                        prio = TempMonsters[i].Prio;
                    }


                }               
                Debug.Log("Mover a " + m.name + " con prio: " + m.Prio);
                yield return null;
                m.ObligarMovimiento();
                TempMonsters.Remove(m);
                while (m.bMoviendo)
                {
                    yield return null;
                }
                Debug.Log("Fin del movimieno de " + m);
            }            
            FasesFinales = FasesFinTurno.AtaqueMon;
        }
        while (FasesFinales == FasesFinTurno.AtaqueMon)
        {
            yield return null;
            FasesFinales = FasesFinTurno.Final;
        }
        while (FasesFinales == FasesFinTurno.Final)
        {
            //Fin del turno
            yield return null;
            bControl = true;                       
            Pos.SetActive(false);
            FasesFinales = FasesFinTurno.Ninguna;             
        }
       
        
       
        yield return null;
    }
    //Comprueba hasta donde puede caminar y coloca en ese lugar su Pos
    public void ComprobarMovimiento(int d) {

        bControl = false;
        Pos.SetActive(true);
        int layermask = LayerMask.GetMask("Obstaculos", "Impasables");
        //Adelante
        if (d == 1) {
            Debug.DrawRay(transform.position, transform.forward * (Velocidad * MoveSpeed), Color.blue, 2);
            RaycastHit R;
            Camino = new Vector3[2];
            if (Physics.Raycast(transform.position, transform.forward, out R, (Velocidad * MoveSpeed), layermask))
            {
                if ((R.point - transform.position).magnitude > 1.25f)
                {
                    Debug.Log("ha chocado con " + R.transform.name + " en el punto: " + R.point);
                    Pos.transform.position = R.point - (R.point - transform.position) * 0.3f;
                    Camino[1] = Pos.transform.position;
                }
                else {
                    //Cant move there
                    Debug.Log("No puedo mover ahi");
                }
               
            }
            else
            {
                Debug.Log("No ha chocado - Muevo hasta " + (transform.position + transform.forward * (Velocidad * MoveSpeed)));
                Pos.transform.position = (transform.position + transform.forward * (Velocidad * MoveSpeed));
                Camino[1] = Pos.transform.position;
            }
        }
        //Atras
        if (d == 2)
        {
            Debug.DrawRay(transform.position, -transform.forward * (Velocidad * MoveSpeed), Color.blue, 2);
            RaycastHit R;
            if (Physics.Raycast(transform.position, -transform.forward, out R, (Velocidad * MoveSpeed), layermask))
            {
                Debug.Log("ha chocado con " + R.transform.name + " en el punto: " + R.point);
                Pos.transform.position = R.point;
            }
            else
            {
                Debug.Log("No ha chocado - Muevo hasta " + (transform.position - (transform.forward * (Velocidad * MoveSpeed))));
                Pos.transform.position = (transform.position - (transform.forward * (Velocidad * MoveSpeed)));
            }
        }
        //Izquierda
        if (d == 4)
        {
            Debug.DrawRay(transform.position, -transform.right * (Velocidad * MoveSpeed), Color.blue, 2);
            RaycastHit R;
            if (Physics.Raycast(transform.position, -transform.right, out R, (Velocidad * MoveSpeed), layermask))
            {
                if ((R.point - transform.position).magnitude > 1.25f)
                {
                    Debug.Log("ha chocado con " + R.transform.name + " en el punto: " + R.point);
                    Pos.transform.position = R.point - (R.point - transform.position) * 0.3f;
                }
                else
                {
                    //Cant move there
                    Debug.Log("No puedo mover ahi");
                }

            }
            else
            {
                Debug.Log("No ha chocado - Muevo hasta " + (transform.position + -transform.right * (Velocidad * MoveSpeed)));
                Pos.transform.position = (transform.position + -transform.right * (Velocidad * MoveSpeed));
            }
        }
        //Movimiento preciso
        if (d == 5)
        {
            Pos.transform.position = Camino[Camino.Length-1];
        }
        EndOfturn.Invoke();
        //Debug.DrawRay(transform.position, transform.forward * 10, Color.blue, 2);
    }
    //Prepara la escena
    public IEnumerator PrepararZona() {
        yield return null;
        //Busca los monstruos de la zona
        CheckMonsters();
        yield return null;
    }
    //Realiza las limpiezas y cambios relevantes para preparar el siguiente turno
    public void PrepararSiguienteTurno() {
        
    }
}
