using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionChecker : MonoBehaviour
{
    public Collider[] Proximos;
    public GameObject[] Chocados;
    public float radius;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CheckPosition(Vector3 p) {
        int layermask = 1 << 6;
        transform.position = p;
        Proximos = Physics.OverlapSphere(transform.position, radius, layermask);
        Chocados = new GameObject[Proximos.Length];
        for(int x = 0; x < Chocados.Length; x++)
        {
            Debug.Log("Chocado con: " + Proximos[x].name);
            Chocados[x] = Proximos[x].gameObject;
        }
    }

}
